/*CURSOR*/
do $$
declare
		cantidad int=0;
		totalprescripcion int;
		registro Record;
		cursor_prescripcion cursor for SELECT*FROM sucursal inner join farmaco on sucursal.suc_id= farmaco.suc_id
		inner join detalleventa on detalleventa.farmaco_id = farmaco.farmaco_id
	inner join vendedor on vendedor.ven_id = detalleventa.ven_id
	inner join venta on venta.venta_id = detalleventa.venta_id
	inner join cliente on cliente.cli_id = detalleventa.cli_id
	WHERE cliente.cli_prescripcion_=TRUE;
begin 
		
		for totalprescripcion in cursor_prescripcion
		loop
		cantidad = cantidad + count(totalprescripcion.cli_prescripcion_);
		
		end loop;
		
Open cursor_prescripcion;
Fetch cursor_prescripcion  into registro;
WHILE(FOUND) LOOP 
Raise notice ' Cliente_nombres: %,  Cliente_apellidos: %,  Cantidad: %  ', registro.cli_nombres, registro.cli_apellidos,cantidad;
Fetch cursor_prescripcion  into registro;
END LOOP;
end $$
language 'plpgsql'; 


