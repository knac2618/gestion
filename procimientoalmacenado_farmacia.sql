
CREATE OR REPLACE FUNCTION DEVOLVER_FARMACOS
		(IN FARMACO_OPCION VARCHAR(40) default 'Anticonceptivos orales', 
		 OUT FARMACO_PRECIO DECIMAL(6,2),
		 OUT FARMACO_STOCK INTEGER,
		 OUT SUC_NOMBRE VARCHAR(20),
		 OUT ACTIVO_CONVENIO BOOLEAN
		) 
		RETURNS setof record AS
$BODY$
BEGIN 
IF FARMACO_OPCION ='Anticonceptivos orales' THEN 
return query SELECT 
farmaco.farmaco_precio,
farmaco.farmaco_stock,
sucursal.suc_nombre,
sucursal.activo_convenio
FROM sucursal inner join farmaco on sucursal.suc_id=farmaco.suc_id
where farmaco.farmaco_nombre='Anticonceptivos orales';
END IF;
IF FARMACO_OPCION ='Heparina' THEN 
return query SELECT 
farmaco.farmaco_precio,
farmaco.farmaco_stock,
sucursal.suc_nombre,
sucursal.activo_convenio
FROM sucursal inner join farmaco on sucursal.suc_id=farmaco.suc_id
where farmaco.farmaco_nombre='Heparina';
END IF;
IF FARMACO_OPCION ='Triamtereno' THEN 
return query SELECT 
farmaco.farmaco_precio,
farmaco.farmaco_stock,
sucursal.suc_nombre,
sucursal.activo_convenio
FROM sucursal inner join farmaco on sucursal.suc_id=farmaco.suc_id
where farmaco.farmaco_nombre='Triamtereno';
END IF;
IF FARMACO_OPCION ='Pelicinamina' THEN 
return query SELECT 
farmaco.farmaco_precio,
farmaco.farmaco_stock,
sucursal.suc_nombre,
sucursal.activo_convenio
FROM sucursal inner join farmaco on sucursal.suc_id=farmaco.suc_id
where farmaco.farmaco_nombre='Pelicinamina';
END IF;
IF FARMACO_OPCION ='Metotrexato' THEN 
return query SELECT 
farmaco.farmaco_precio,
farmaco.farmaco_stock,
sucursal.suc_nombre,
sucursal.activo_convenio
FROM sucursal inner join farmaco on sucursal.suc_id=farmaco.suc_id
where farmaco.farmaco_nombre='Metotrexato';
END IF;
IF FARMACO_OPCION ='Colestipol' THEN 
return query SELECT 
farmaco.farmaco_precio,
farmaco.farmaco_stock,
sucursal.suc_nombre,
sucursal.activo_convenio
FROM sucursal inner join farmaco on sucursal.suc_id=farmaco.suc_id
where farmaco.farmaco_nombre='Colestipol';
END IF;
return;
END;
$BODY$
language 'plpgsql'; 

SELECT * FROM public.DEVOLVER_FARMACOS('Metotrexato');
