
/* Insercion de estado vendedor */
INSERT INTO estadovendedor VALUES (1, 'Activo');
INSERT INTO estadovendedor VALUES (2, 'Inactivo');

/* Insercion de datos de horario laboral */
INSERT INTO horariolaboral VALUES (1,'08:00:00', '17:00:00', 3.50, 2.50,100);
INSERT INTO horariolaboral VALUES (2,'17:00:00', '23:55:00', 4.50, 3.50,120);

/* Insercion de datos de cliente */
INSERT INTO cliente VALUES (1,'1313528517', 'José María', 'Loor Mera', 'calle 11 Av 12' , '0985123888', 'j_loor@gamil.com', '14/05/2000',TRUE);
INSERT INTO cliente VALUES (2,'1314828633', 'Carlos Pepe', 'Bailón Mero', 'Barrio Los ceibos' , '0985067188', 'c_bailon@gamil.com', '22/07/2000',FALSE);
INSERT INTO cliente VALUES (3,'1314528666', 'Steven Pablo', 'Loor Mera', 'Barrio Los ceibos' , '0985067888', 's_loor@gamil.com', '13/05/2001',TRUE);
INSERT INTO cliente VALUES (4,'1314144010', 'Lucia Antonia', 'Anchundia Delgado', 'Barrio Los ceibos' , '0985067888', 'l_anchundiar@gamil.com', '14/05/2000',FALSE);
INSERT INTO cliente VALUES (5,'1333528502', 'María Karla', 'Pérez Cedeño', 'Barrio San Agustin' , '0985064188', 'm_perez@gamil.com', '05/09/1999',TRUE);
INSERT INTO cliente VALUES (6,'1325937203', 'Rosa Maryuri', 'Cañarte Garcia', 'Barrio 2 de Agosto' , '0990032795', 'maryuita23@gamil.com', '23/04/1972',FALSE);
INSERT INTO cliente VALUES (7,'1312208722', 'Carlos Javier ', 'Valencia Ozaeta', 'calle 14 av 25 y 26' , '0996413094', 'carlos0218@gamil.com', '02/04/2000',TRUE);
INSERT INTO cliente VALUES (8,'1307789998', 'Narcisa Jaqueline', 'Parrales Moreira', 'Barrio 26 de septiembre' , '0926485086', 'narci_pa@gamil.com', '02/06/1962',TRUE);

/* Insercion de datos de sucursal */
INSERT INTO sucursal VALUES (1,'Amaia Moreira','Av 17 calle 13', '0978471254',TRUE);

/* Insercion de datos de farmaco */
INSERT INTO farmaco VALUES (1,1,'Anticonceptivos orales', 5.99, '12/05/2023',200);
INSERT INTO farmaco VALUES (2,1,'Heparina', 5.99, '12/05/2023', 150);
INSERT INTO farmaco VALUES (3,1,'Triamtereno', 4.99, '12/05/2023',300);
INSERT INTO farmaco VALUES (4,1,'Pelicinamina', 3.99, '12/05/2023',100);
INSERT INTO farmaco VALUES (5,1,'Metotrexato', 3.99, '12/05/2023',50);
INSERT INTO farmaco VALUES (6,1,'Colestipol', 7.99, '12/05/2023',250);
	
/* Insercion de datos de vendedor */
INSERT INTO vendedor VALUES (1, 1, 1, 1, 'Pablo Andrés', 'Pinoargote Mera', '14/03/1985', '22/01/2012', 0);
INSERT INTO vendedor VALUES (2, 1, 1, 2, 'Mario José', 'Pilligua Gómez', '13/04/1996', '22/01/2013', 1);
INSERT INTO vendedor VALUES (3, 1, 1, 1, 'Emilia Laura', 'Zambrano Cañarte', '11/08/1997', '22/11/2014', 2);
INSERT INTO vendedor VALUES (4, 1, 2, 1, 'Carlos Alfredo', 'Barreiro Pérez', '14/12/1998', '22/01/2015', 2);
INSERT INTO vendedor VALUES (5, 1, 1, 1, 'Jose Luis', 'Pachay Gonzalez', '13/02/1998', '26/01/2015', 1);
INSERT INTO vendedor VALUES (6, 1, 1, 2, 'Daniel Octavio', 'Palma Plaza', '16/05/1997', '06/10/2016', 5);
INSERT INTO vendedor VALUES (7, 1, 2, 2, 'Cristina Johanna', 'Ozaeta Macias', '11/11/1999', '29/01/2016', 5);
	
/* Insercion de datos de venta */
INSERT INTO venta VALUES (1, '11/11/2020', null);
INSERT INTO venta VALUES (2,'22/01/2021', 1.99);
INSERT INTO venta VALUES (3, '15/07/2021', 1.99);
INSERT INTO venta VALUES (4, '08/05/2021', null);
INSERT INTO venta VALUES (5, '15/12/2021', 1.99);
	
/* Insercion de datos de destalle de venta */
INSERT INTO detalleventa VALUES (1,1,1,1,1,5);
INSERT INTO detalleventa VALUES (2,2,3,1,1,9);
INSERT INTO detalleventa VALUES (3,3,3,1,2,4);
INSERT INTO detalleventa VALUES (4,4,4,1,1,2);
INSERT INTO detalleventa VALUES (5,5,5,1,4,6);
	
/* Insercion de datos de farmaceutica */

INSERT INTO farmaceutica VALUES (1,'Moderna','Av 15 calle 17','0989212184');
INSERT INTO farmaceutica VALUES (2,'C-Logistica','Av Interbarrial Sur','0989789184');
INSERT INTO farmaceutica VALUES (3,'F-Merck','Av 12 calle 20','0989212412');
INSERT INTO farmaceutica VALUES (4,'F-Mana','calle 108 av 23', '0963521907');
INSERT INTO farmaceutica VALUES (5,'fredekir','Av 15 calle 13', '0924938710');
/* Insercion de datos de proveedor */

INSERT INTO proveedor VALUES (1,1,'Grupo Lucus','Av 17 calle 17','0939212104', 'g_lucus@hotmail.com');
INSERT INTO proveedor VALUES (2,2,'CBD Hispana','Av 12 calle 11','0949424024', 'cbd_hispana@hotmail.com');
INSERT INTO proveedor VALUES (3,3,'Maxin farma','Av 89 calle 74','0987892184', 'm_farma@hotmail.com');
INSERT INTO proveedor VALUES (4,2,'propifarma',' av 25 calle 25','0900671928', 'propifarma@hotmail.com');
INSERT INTO proveedor VALUES (5,3,'Marco Farmaci','Av 36 calle 14','0946252391', 'marcofarmaci@hotmail.com');
	
/* Insercion de datos de adquisicion de farmacos */
INSERT INTO adquisicionfarmaco VALUES (1, 1, 1, '12/03/2020', 11.39);
INSERT INTO adquisicionfarmaco VALUES (2, 2, 1, '12/03/2020', 12.49);
INSERT INTO adquisicionfarmaco VALUES (3, 3, 2, '15/03/2020', 11.99);
INSERT INTO adquisicionfarmaco VALUES (4, 4, 3, '20/03/2020', 12.59);
INSERT INTO adquisicionfarmaco VALUES (5, 5, 2, '15/03/2021', 15.99);
INSERT INTO adquisicionfarmaco VALUES (6, 6, 2, '15/03/2021', 15.99);